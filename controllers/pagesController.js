const categoryModel = require("../models/Category");
const bankModel = require("../models/Bank");
const itemModel = require("../models/Item");
const userModel = require("../models/Users");
const featureModel = require("../models/Feature");
const activityModel = require("../models/Activity");
const bookingModel = require("../models/Booking");
const memberModel = require("../models/Member");
const bcrypt = require("bcryptjs");

module.exports = {
  signinView: async (req, res) => {
    try {
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      if (req.session.user == null || req.session.user == undefined) {
        res.render("index", {
          alert,
          title: "Staycation | Login",
        });
      } else {
        res.redirect("/pages/dashboard");
      }
    } catch (error) {
      res.redirect("/pages/signin");
    }
  },
  actionSignin: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await userModel.findOne({ username: username });
      if (!user) {
        req.flash("alertMessage", "User yang anda masukan tidak ada!!");
        req.flash("alertStatus", "danger");
        res.redirect("/pages/signin");
      }
      const isPasswordMatch = await bcrypt.compare(password, user.password);
      if (!isPasswordMatch) {
        req.flash("alertMessage", "Password yang anda masukan tidak cocok!!");
        req.flash("alertStatus", "danger");
        res.redirect("/pages/signin");
      }

      req.session.user = {
        id: user.id,
        username: user.username,
      };
      res.redirect("/pages/dashboard");
    } catch (error) {
      res.redirect("/pages/signin");
    }
  },
  actionLogout: (req, res) => {
    req.session.destroy();
    res.redirect("/pages/signin");
  },
  dashboardView: async (req, res) => {
    try{
      const member = await memberModel.find();
      const booking = await bookingModel.find();
      const item = await itemModel.find();
      res.render('pages/dashboard/dashboard_view', {
        title: "Staycation | Dashboard",
        user: req.session.user,
        member,
        booking,
        item
      });
    } catch (error) {
      res.redirect('/admin/dashboard');
    }
  },
  categoryView: async (req, res) => {
    try {
      const data = await categoryModel.find();
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      res.render("pages/category/category_view", {
        data,
        alert,
        title: "Staycation | Category",
        user: req.session.user,
      });
    } catch (error) {
      res.render("pages/category/category_view", {
        title: "Staycation | Category",
      });
    }
  },
  bankView: async (req, res) => {
    try {
      const data = await bankModel.find();
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      res.render("pages/bank/bank_view", {
        data,
        req,
        alert,
        title: "Staycation | Bank",
        user: req.session.user,
      });
    } catch (error) {
      res.render("pages/bank/bank_view", { title: "Staycation | Bank" });
    }
  },
  itemView: async (req, res) => {
    try {
      const data = await itemModel
        .find()
        .populate({ path: "imageId", select: "id imageUrl" })
        .populate({ path: "categoryId", select: "id name" });
      const category = await categoryModel.find();
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      res.render("pages/item/item_view", {
        data,
        category,
        alert,
        title: "Staycation | Item",
        user: req.session.user,
        action: "view",
      });
    } catch (error) {
      res.render("pages/item/item_view", { title: "Staycation | Item" });
    }
  },
  itemImageShow: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await itemModel
        .findOne({ _id: id })
        .populate({ path: "imageId", select: "id imageUrl" });
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      res.render("pages/item/item_view", {
        data,
        alert,
        title: "Staycation | Show Image Item",
        action: "show image",
        user: req.session.user,
        req,
      });
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/item");
    }
  },
  itemEdit: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await itemModel
        .findOne({ _id: id })
        .populate({ path: "imageId", select: "id imageUrl" })
        .populate({ path: "categoryId", select: "id name" });
      const category = await categoryModel.find();
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      console.log(data);
      res.render("pages/item/item_view", {
        data,
        alert,
        title: "Staycation | Edit Item",
        action: "show edit",
        category,
        user: req.session.user,
        req,
      });
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/item");
    }
  },
  itemDetailView: async (req, res) => {
    const { itemId } = req.params;
    try {
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };

      const feature = await featureModel.find({ itemId: itemId });
      const activity = await activityModel.find({ itemId: itemId });

      res.render("pages/item/item_detail/item_detail_view", {
        title: "Staycation | Detail Item",
        alert,
        itemId,
        feature,
        activity,
        req,
        user: req.session.user,
      });
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },
  bookingView: async (req, res) => {
    try {
      const data = await bookingModel
        .find()
        .populate("memberId")
        .populate("bankId");

      res.render("pages/booking/booking_view", {
        title: "Staycation | Booking",
        user: req.session.user,
        data,
      });
    } catch (error) {
      console.log(error.message);
      res.redirect("/pages/booking");
    }
  },
  bookingDetailShow: async (req, res) => {
    const { id } = req.params;
    try {
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };

      const data = await bookingModel
        .findOne({ _id: id })
        .populate("memberId")
        .populate("bankId");

      res.render("pages/booking/booking_detail", {
        title: "Staycation | Detail Booking",
        user: req.session.user,
        data,
        alert,
        req,
      });
    } catch (error) {
      res.redirect("/pages/booking");
    }
  },
};
