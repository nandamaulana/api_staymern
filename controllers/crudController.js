const Category = require("../models/Category");
const Bank = require("../models/Bank");
const Item = require("../models/Item");
const Feature = require("../models/Feature");
const Activity = require("../models/Activity");
const Image = require("../models/Image");
const Booking = require("../models/Booking");
const fs = require("fs-extra");
const path = require("path");

module.exports = {
  categoryAdd: async (req, res) => {
    try {
      const { name } = req.body;
      await Category.create({ name });
      req.flash("alertMessage", "Success Add Category");
      req.flash("alertStatus", "success");
      res.redirect("/pages/category");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/category");
    }
  },
  categoryUpdate: async (req, res) => {
    try {
      const { id, name } = req.body;
      const category = await Category.findOne({ _id: id });
      category.name = name;
      await category.save();
      req.flash("alertMessage", "Success Edit Category");
      req.flash("alertStatus", "success");
      res.redirect("/pages/category");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/category");
    }
  },
  categoryDelete: async (req, res) => {
    try {
      const { id } = req.params;
      const category = await Category.findOne({ _id: id });
      await category.remove();
      req.flash("alertMessage", "Success Delete Category");
      req.flash("alertStatus", "success");
      res.redirect("/pages/category");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/category");
    }
  },
  bankAdd: async (req, res) => {
    try {
      const { nameBank, nomorRekening, nameOwner } = req.body;

      await Bank.create({
        nameBank,
        nomorRekening,
        nameOwner,
        imageUrl: `images/${req.file.filename}`,
      });
      req.flash("alertMessage", "Success Add Bank");
      req.flash("alertStatus", "success");
      res.redirect("/pages/bank");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/bank");
    }
  },
  bankUpdate: async (req, res) => {
    try {
      const { id, nameBank, nomorRekening, nameOwner } = req.body;
      const data = await Bank.findOne({ _id: id });
      if (req.file !== undefined) {
        await fs.unlink(path.join(`public/${data.imageUrl}`));
        data.imageUrl = `images/${req.file.filename}`;
      }
      data.nameBank = nameBank;
      data.nomorRekening = nomorRekening;
      data.nameOwner = nameOwner;
      await data.save();
      req.flash("alertMessage", "Success Edit Bank");
      req.flash("alertStatus", "success");
      res.redirect("/pages/bank");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/bank");
    }
  },
  bankDelete: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await Bank.findOne({ _id: id });
      await fs.unlink(path.join(`public/${data.imageUrl}`));
      await data.remove();
      req.flash("alertMessage", "Success Delete Bank");
      req.flash("alertStatus", "success");
      res.redirect("/pages/bank");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/bank");
    }
  },
  itemAdd: async (req, res) => {
    try {
      const { title, price, city, categoryId, about } = req.body;
      if (req.files.length > 0) {
        const category = await Category.findOne({ _id: categoryId });
        const newItem = {
          title,
          price,
          city,
          categoryId,
          description: about,
        };
        const item = await Item.create(newItem);
        category.itemId.push({ _id: item._id });
        await category.save();

        for (let i = 0; i < req.files.length; i++) {
          const imageSave = await Image.create({
            imageUrl: `images/${req.files[i].filename}`,
          });
          item.imageId.push({ _id: imageSave._id });
          await item.save();
        }
        req.flash("alertMessage", "Success Add Item");
        req.flash("alertStatus", "success");
        res.redirect("/pages/item");
      }
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/item");
    }
  },
  itemUpdate: async (req, res) => {
    try {
      const { id, title, price, city, categoryId, about } = req.body;
      const data = await Item.findOne({ _id: id })
        .populate({ path: "categoryId", select: "id name" })
        .populate({ path: "imageId", select: "id imageUrl" });

      if (req.files.length > 0) {
        for (let i = 0; i < data.imageId.length; i++) {
          const imageUpdate = await Image.findOne({ _id: data.imageId[i]._id });
          await fs.unlink(path.join(`public/${imageUpdate.imageUrl}`));
          await Image.findOneAndDelete({ _id: data.imageId[i]._id });
        }
        for (let i = 0; i < req.files.length; i++) {
          const imageSave = await Image.create({
            imageUrl: `images/${req.files[i].filename}`,
          });
          data.imageId.push({ _id: imageSave._id });
          await data.save();
        }
      }
      data.title = title;
      data.price = price;
      data.city = city;
      data.categoryId = categoryId;
      data.description = about;
      await data.save();
      req.flash("alertMessage", "Success Edit Item");
      req.flash("alertStatus", "success");
      res.redirect("/pages/item");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/item");
    }
  },
  itemDelete: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await Item.findOne({ _id: id }).populate("imageId");
      for (let i = 0; i < data.imageId.length; i++) {
        Image.findOne({ _id: data.imageId[i]._id })
          .then((image) => {
            fs.unlink(path.join(`public/${image.imageUrl}`));
            image.remove();
          })
          .catch((error) => {
            req.flash("alertMessage", `${error.message}`);
            req.flash("alertStatus", "danger");
            res.redirect("/pages/item");
          });
      }
      await data.remove();
      req.flash("alertMessage", "Success Delete Item");
      req.flash("alertStatus", "success");
      res.redirect("/pages/item");
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/pages/item");
    }
  },
  featureAdd: async (req, res) => {
    const { name, qty, itemId } = req.body;

    try {
      if (!req.file) {
        req.flash("alertMessage", "Image not found");
        req.flash("alertStatus", "danger");
        res.redirect(`/pages/item/item-detail-show/${itemId}`);
      }
      const feature = await Feature.create({
        name,
        qty,
        itemId,
        imageUrl: `images/${req.file.filename}`,
      });

      const item = await Item.findOne({ _id: itemId });
      item.featureId.push({ _id: feature._id });
      await item.save();
      req.flash("alertMessage", "Success Add Feature");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },

  featureEdit: async (req, res) => {
    const { id, name, qty, itemId } = req.body;
    try {
      const feature = await Feature.findOne({ _id: id });
      if (req.file == undefined) {
        feature.name = name;
        feature.qty = qty;
        await feature.save();
        req.flash("alertMessage", "Success Update Feature");
        req.flash("alertStatus", "success");
        res.redirect(`/pages/item/item-detail-show/${itemId}`);
      } else {
        await fs.unlink(path.join(`public/${feature.imageUrl}`));
        feature.name = name;
        feature.qty = qty;
        feature.imageUrl = `images/${req.file.filename}`;
        await feature.save();
        req.flash("alertMessage", "Success Update Feature");
        req.flash("alertStatus", "success");
        res.redirect(`/pages/item/item-detail-show/${itemId}`);
      }
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },

  featureDelete: async (req, res) => {
    const { id, itemId } = req.params;
    try {
      const feature = await Feature.findOne({ _id: id });

      const item = await Item.findOne({ _id: itemId }).populate("featureId");
      for (let i = 0; i < item.featureId.length; i++) {
        if (item.featureId[i]._id.toString() === feature._id.toString()) {
          item.featureId.pull({ _id: feature._id });
          await item.save();
        }
      }
      await fs.unlink(path.join(`public/${feature.imageUrl}`));
      await feature.remove();
      req.flash("alertMessage", "Success Delete Feature");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },
  activityAdd: async (req, res) => {
    const { name, type, itemId } = req.body;

    try {
      if (!req.file) {
        req.flash("alertMessage", "Image not found");
        req.flash("alertStatus", "danger");
        res.redirect(`/pages/item/item-detail-show/${itemId}`);
      }
      const activity = await Activity.create({
        name,
        type,
        itemId,
        imageUrl: `images/${req.file.filename}`,
      });
      const item = await Item.findOne({ _id: itemId });
      item.activityId.push({ _id: activity._id });
      await item.save();
      req.flash("alertMessage", "Success Add Activity");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },

  activityEdit: async (req, res) => {
    const { id, name, type, itemId } = req.body;
    try {
      const activity = await Activity.findOne({ _id: id });
      if (req.file !== undefined) {
        await fs.unlink(path.join(`public/${activity.imageUrl}`));
        activity.imageUrl = `images/${req.file.filename}`;
      }
      activity.name = name;
      activity.type = type;
      await activity.save();
      req.flash("alertMessage", "Success Update activity");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },

  activityDelete: async (req, res) => {
    const { id, itemId } = req.params;
    try {
      const activity = await Activity.findOne({ _id: id });

      const item = await Item.findOne({ _id: itemId }).populate("activityId");
      for (let i = 0; i < item.activityId.length; i++) {
        if (item.activityId[i]._id.toString() === activity._id.toString()) {
          item.activityId.pull({ _id: activity._id });
          await item.save();
        }
      }
      await fs.unlink(path.join(`public/${activity.imageUrl}`));
      await activity.remove();
      req.flash("alertMessage", "Success Delete Activity");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    } catch (error) {
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect(`/pages/item/item-detail-show/${itemId}`);
    }
  },

  actionConfirmation: async (req, res) => {
    const { id } = req.params;
    try {
      const booking = await Booking.findOne({ _id: id });
      booking.payments.status = "Accept";
      await booking.save();
      req.flash("alertMessage", "Success Confirmation Pembayaran");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/booking/${id}`);
    } catch (error) {
      res.redirect(`/pages/booking/${id}`);
    }
  },

  actionReject: async (req, res) => {
    const { id } = req.params;
    try {
      const booking = await Booking.findOne({ _id: id });
      booking.payments.status = "Reject";
      await booking.save();
      req.flash("alertMessage", "Success Reject Pembayaran");
      req.flash("alertStatus", "success");
      res.redirect(`/pages/booking/${id}`);
    } catch (error) {
      res.redirect(`/pages/booking/${id}`);
    }
  },
};
