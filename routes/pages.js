const router = require("express").Router();
const pagesController = require("../controllers/pagesController");
const auth = require('../middlewares/auth')

router.get("/signin", pagesController.signinView);
router.post("/signin", pagesController.actionSignin);
router.use(auth);
router.get("/logout", pagesController.actionLogout);
router.get("/dashboard", pagesController.dashboardView);
router.get("/category", pagesController.categoryView);
router.get("/bank", pagesController.bankView);
router.get("/item", pagesController.itemView);
router.get("/item/:id", pagesController.itemEdit);
router.get("/item/show-image/:id", pagesController.itemImageShow);
router.get("/booking", pagesController.bookingView);
router.get("/booking/:id", pagesController.bookingDetailShow);

router.get("/item/item-detail-show/:itemId", pagesController.itemDetailView);

module.exports = router;
