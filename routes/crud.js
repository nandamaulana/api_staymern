const router = require("express").Router();
const crudController = require("../controllers/crudController");
const { uploadMultiple, upload } = require("../middlewares/multer");

//category
router.post("/category", crudController.categoryAdd);
router.put("/category", crudController.categoryUpdate);
router.delete("/category/:id", crudController.categoryDelete);
//category
router.post("/bank", upload, crudController.bankAdd);
router.put("/bank", upload, crudController.bankUpdate);
router.delete("/bank/:id", crudController.bankDelete);
//item
router.post("/item", uploadMultiple, crudController.itemAdd);
router.put("/item", uploadMultiple, crudController.itemUpdate);
router.delete("/item/:id/delete", crudController.itemDelete);
//item detail
//feature
router.post("/item/add/feature", upload, crudController.featureAdd);
router.put("/item/update/feature", upload, crudController.featureEdit);
router.delete("/item/:itemId/feature/:id", crudController.featureDelete);
//activity
router.post("/item/add/activity", upload, crudController.activityAdd);
router.put("/item/update/activity", upload, crudController.activityEdit);
router.delete("/item/:itemId/activity/:id", crudController.activityDelete);
// booking
router.put('/booking/:id/confirmation', crudController.actionConfirmation);
router.put('/booking/:id/reject', crudController.actionReject);


module.exports = router;
